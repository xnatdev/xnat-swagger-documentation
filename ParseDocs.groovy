import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import org.nrg.xnat.doc.ApiDefinition
import org.nrg.xnat.doc.ApiEndpoint
import org.nrg.xnat.doc.ApiInfo
import org.nrg.xnat.doc.ApiJson
import org.nrg.xnat.doc.ApiSchema
import org.nrg.xnat.doc.ApiTag

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

@Grapes([
        @Grab('com.fasterxml.jackson.core:jackson-annotations:2.8.2'),
        @Grab('com.fasterxml.jackson.core:jackson-core:2.8.2'),
        @Grab('com.fasterxml.jackson.core:jackson-databind:2.8.2')
])
cli = new CliBuilder(usage: 'groovy ParseDocs.groovy -u wikiUsername -p wikiPassword -f jsonFile.json')

cli.with {
    u( longOpt: 'user', 'Username for wiki account', args: 1, required: true)
    p( longOpt: 'pass', 'Password for the user', args: 1, required: true)
    f( longOpt: 'file', 'File containing the full XAPI JSON', args: 1, required: true)
}

if ('-h' in args || '--help' in args) {
    cli.usage()
    return
}

def params = cli.parse(args)
final String user = params.u
final String password = params.p
final String fullJsonFile = params.f

final ObjectMapper objectMapper = new ObjectMapper()
objectMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
final ApiJson fullApi = objectMapper.readValue(new File(fullJsonFile), ApiJson)
final List<ApiJson> splitApis = []
fullApi.tags.each { tag ->
    final ApiJson api = new ApiJson()
    api.setSwagger(fullApi.swagger)
    api.setInfo(new ApiInfo().version(fullApi.info.version).title(tag.description).description(tag.description))
    api.setTags([tag])
    splitApis << api
}

fullApi.paths.each { url, calls ->
    splitApis.find{ it.tags[0].name in calls.findAnyCall().tags }.paths << [(url) : calls]
}

fullApi.definitions.each { definitionName, definition ->
    definition.props.each { propName, value ->
        if (value.definitionReference != null) {
            definition.dependsOn << value.definitionReference
        }
        if (value.items != null && value.items.definitionReference != null) {
            definition.dependsOn << value.items.definitionReference
        }
        if (value.additionalProperties != null && value.additionalProperties.definitionReference != null) {
            definition.dependsOn << value.additionalProperties.definitionReference
        }
    }
}

splitApis.each { api ->
    final Set<String> includedDefinitions = []
    api.paths.values().each {
        it.findAllCalls().each { call ->
            (call.parameters + call.responses.values()).each { param ->
                final ApiSchema schema = param.schema
                if (schema != null) {
                    if (schema.definitionLink != null) {
                        includedDefinitions << schema.definitionLink
                    }
                    if (schema.additionalProperties != null && schema.additionalProperties.definitionReference != null) {
                        includedDefinitions << schema.additionalProperties.definitionReference
                    }
                    if (schema.items != null && schema.items.containsKey('$ref')) {
                        includedDefinitions << schema.items.get('$ref')
                    }
                }
            }
        }
    }

    final Map<String, ApiDefinition> definitionMap = api.definitions
    includedDefinitions.each { definitionKey ->
        final String splitKey = definitionKey.split('/').last()
        final ApiDefinition definition = fullApi.definitions.get(splitKey)
        if (definition != null) {
            definitionMap << [(splitKey) : definition]
            definition.dependsOn.each { unsplitKey ->
                final String depdendencyKey = unsplitKey.split('/').last()
                definitionMap << [(depdendencyKey) : fullApi.definitions.get(depdendencyKey)]
            }
        } else {
            println "WARNING: unknown definition with key ${splitKey} being used in API '${api.info.title}'. An empty definition will be created to hide errors in the wiki."
            final ApiDefinition fakeDefinition = new ApiDefinition()
            fakeDefinition.setType('object')
            definitionMap << [(splitKey) : fakeDefinition]
        }
    }
}

final Path output = Paths.get('output')
if (output.toFile().exists()) {
    output.toFile().listFiles().each { file ->
        file.delete()
    }
} else {
    Files.createDirectory(output)
}

new File('external').listFiles().each { file ->
    Files.copy(file.toPath(), output.resolve(file.name))
}

final Map<String, String> knownWikiPages = new File('wikiTagMap.txt').readLines().collectEntries { [(it.split(':')[0]) : it.split(':')[1]] }

splitApis.each { api ->
    final String apiKey = api.tags[0].name
    final String json = objectMapper.writeValueAsString(api)
    output.resolve("${apiKey}.json").toFile() << json
    if (knownWikiPages.containsKey(apiKey)) {
        output.resolve("${apiKey}.md").toFile() << output.resolve('api.md.template').text.replace('%SWAGGER_JSON_HERE%', json).replace('%PAGE_ID%', knownWikiPages[apiKey])
        final StringBuffer stdOut = new StringBuffer()
        final StringBuffer stdErr = new StringBuffer()

        final Process htmlConvert = "./md2confluencehtml.sh ${apiKey}.md".execute([], output.toFile())
        htmlConvert.consumeProcessOutput(stdOut, stdErr)
        htmlConvert.waitForOrKill(10000)
        if (stdOut) {
            println stdOut
            stdOut.delete(0, stdOut.length())
        }
        if (stdErr) {
            println stdErr
            stdErr.delete(0, stdErr.length())
        }

        final Process confluenceUpdate = "./upload-confluence.py ${user} ${password} ${apiKey}.html.confluence".execute([], output.toFile())
        confluenceUpdate.consumeProcessOutput(stdOut, stdErr)
        confluenceUpdate.waitForOrKill(10000)
        if (stdOut) println stdOut
        if (stdErr) println stdErr
    }
}