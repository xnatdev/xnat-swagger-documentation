package org.nrg.xnat.doc

import com.fasterxml.jackson.annotation.JsonProperty

class ApiProperty {
    String type
    String description
    String format
    ApiProperty items
    ApiProperty additionalProperties
    @JsonProperty('enum') List<String> enumList
    @JsonProperty('$ref') String definitionReference
}
