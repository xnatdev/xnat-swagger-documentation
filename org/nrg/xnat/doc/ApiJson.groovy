package org.nrg.xnat.doc

import com.fasterxml.jackson.annotation.JsonIgnore

class ApiJson {
    String swagger
    ApiInfo info
    List<ApiTag> tags = []
    Map<String, ApiEndpoint> paths = [:]
    Map<String, ApiDefinition> definitions = [:]
    @JsonIgnore String host
    @JsonIgnore String basePath
}
