package org.nrg.xnat.doc

import com.fasterxml.jackson.annotation.JsonProperty

class ApiParameter {
    @JsonProperty('in') String paramIn
    String name
    String description
    Boolean required
    ApiSchema schema
    String type
    @JsonProperty('default') Object defaultValue
    String format
}