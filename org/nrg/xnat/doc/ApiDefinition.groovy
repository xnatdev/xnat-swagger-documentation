package org.nrg.xnat.doc

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty

class ApiDefinition {
    String type
    String description
    @JsonProperty('properties') Map<String, ApiProperty> props
    ApiProperty additionalProperties
    @JsonIgnore Set<String> dependsOn = []
}
