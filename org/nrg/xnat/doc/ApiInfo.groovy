package org.nrg.xnat.doc

import com.fasterxml.jackson.annotation.JsonIgnore

class ApiInfo {
    String description
    String version
    String title
    @JsonIgnore String termsOfService
    @JsonIgnore Object contact
    @JsonIgnore Object license

    ApiInfo description(String description) {
        setDescription(description)
        this
    }

    ApiInfo version(String version) {
        setVersion(version)
        this
    }

    ApiInfo title(String title) {
        setTitle(title)
        this
    }
}
