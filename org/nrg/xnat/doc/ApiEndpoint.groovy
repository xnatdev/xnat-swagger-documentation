package org.nrg.xnat.doc

class ApiEndpoint {
    ApiCall get
    ApiCall post
    ApiCall put
    ApiCall delete

    ApiCall findAnyCall() {
        get ?: (post ?: (put ?: delete))
    }

    List<ApiCall> findAllCalls() {
        final List<ApiCall> calls = []
        if (get != null) calls << get
        if (post != null) calls << post
        if (put != null) calls << put
        if (delete != null) calls << delete
        calls
    }
}
