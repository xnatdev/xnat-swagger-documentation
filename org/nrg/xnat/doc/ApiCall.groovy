package org.nrg.xnat.doc

class ApiCall {
    List<String> tags = []
    String summary
    String description
    String operationId
    List<String> consumes = []
    List<String> produces = []
    List<ApiParameter> parameters = []
    Map<String, ApiResponse> responses = [:]
}
