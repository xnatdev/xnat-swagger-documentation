package org.nrg.xnat.doc

import com.fasterxml.jackson.annotation.JsonProperty

class ApiSchema {
    String type
    @JsonProperty('$ref') String definitionLink
    Map<String, String> items = [:]
    ApiProperty additionalProperties
}